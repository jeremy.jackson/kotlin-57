package currency.conversion

class CurrencyConverter() {
  val euros: Double
  val rate: Double
  val dollars: Double
  val message: String

  init {
    print("How many Euros are you exchanging? ")
    euros = readLine()?.toDouble() ?: 0.toDouble()
    print("What is the exchange rate? ")
    rate = readLine()?.toDouble() ?: 0.toDouble()
    dollars = convert(euros, rate, 100.0)
    message = writeMessage(euros, rate, dollars)
  }

  fun convert(amtFrom: Double, rateFrom: Double, rateTo: Double): Double {
    return (amtFrom * rateFrom) / rateTo
  }

  fun writeMessage(euros: Double, rate: Double, dollars: Double): String {
    return """
      |%.0f euros at an exchange rate of %.2f is
      |%.2f U.S. dollars
    """.trimMargin().format(euros, rate, dollars)
  }

  fun say() {
    println(message)
  }
}
