package printing.quotes

class Quotes {
  var quote: String
  var speaker: String

  init {
    print("What is the quote? ")
    this.quote = reader()
    print("Who said it? ")
    this.speaker = reader()
  }

  fun reader(): String {
    val output = readLine() ?: ""
    return output
  }

  fun say() {
    val message = this.speaker + " says, \"" + this.quote + "\""
    println(message)
  }
}
