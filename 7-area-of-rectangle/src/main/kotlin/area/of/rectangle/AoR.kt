package area.of.rectangle

val F2MConvFactor = 0.09290304

class AoR {
  val length: Float
  val width: Float
  var areaFeet: Float = 0.toFloat()
  var areaMetric: Float = 0.toFloat()

  init {
    print("What is the length of the room in feet? ")
    this.length = reader() ?: 0.toFloat()
    print("What is the width of the room in feet? ")
    this.width = reader() ?: 0.toFloat()
    calcAreas()
  }

  fun reader(): Float? {
    var num = rl()
    return num
  }

  fun rl(): Float? {
    var num: Float? = 0.toFloat()
    try {
      num = readLine()?.toFloat()
    } catch (e: NumberFormatException) {
      num = retry()
    }
    return num
  }

  fun retry(): Float? {
    print("Please provide a number: ")
    val num = rl()
    return num
  }

  fun calcAreas() {
    val (length, width) = this
    this.areaFeet = length * width
    this.areaMetric = this.areaFeet * F2MConvFactor.toFloat()
  }

  fun say() {
    val (length, width, areaFeet, areaMetric) = this
    println("""
      |You entered dimensions of $length feed by $width feet.
      |The area is
      |$areaFeet square feet
      |$areaMetric square meters
    """.trimMargin())
  }

  operator fun component1() = this.length
  operator fun component2() = this.width
  operator fun component3() = this.areaFeet
  operator fun component4() = this.areaMetric
}
