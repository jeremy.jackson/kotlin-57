package computing.simple.interest

class ComputeSimpleInterest {
  val principal: Float
  val prerate: Float
  val rate: Float
  val years: Float
  var amount: Float = 0.toFloat()

  init {
    print("Enter the principal: ")
    principal = readLine()?.toFloat() ?: 0.toFloat()
    print("Enter the rate of interest: ")
    prerate = readLine()?.toFloat() ?: 0.toFloat()
    rate = prerate / 100
    print("Enter the number of years: ")
    years = readLine()?.toFloat() ?: 0.toFloat()
  }

  fun calculate() {
    amount = principal * (1 + rate * years)
  }

  fun say() {
    println("""
      |After %.0f years at ${prerate}%% interest,
      |the investment will be worth $%.2f.
    """.trimMargin().format(years, amount))
  }
}
