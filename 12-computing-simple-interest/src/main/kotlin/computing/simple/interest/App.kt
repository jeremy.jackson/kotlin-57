/*
 * This Kotlin source file was generated by the Gradle 'init' task.
 */
package computing.simple.interest

fun main(args: Array<String>) {
  val si = ComputeSimpleInterest()
  si.calculate()
  si.say()
}
