package simple.math

class Calcs() {
  val n1: Float
  val n2: Float

  init {
    print("What is the first number? ")
    n1 = reader() ?: 0.toFloat()
    print("What is the second number? ")
    n2 = reader() ?: 0.toFloat()
  }

  fun reader(): Float? {
    var num = rl()
    return num
  }

  fun rl(): Float? {
    var num: Float? = 0.toFloat()
    try {
      num = readLine()?.toFloat()
    } catch (e: NumberFormatException) {
      num = retry()
    }
    return num
  }

  fun retry(): Float? {
    print("Please provide a number: ")
    val num = rl()
    return num
  }

  fun say() {
    val (n1, n2) = this
    println("""
      |$n1 + $n2 = ${n1 + n2}
      |$n1 - $n2 = ${n1 - n2}
      |$n1 * $n2 = ${n1 * n2}
      |$n1 / $n2 = ${n1 / n2}
      """.trimMargin())
  }

  operator fun component1() = this.n1
  operator fun component2() = this.n2
}
