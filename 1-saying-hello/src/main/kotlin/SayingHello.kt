package saying.hello

fun Greeting() = {
  print("What is your name? ")
  val name = readLine()
  println("\n$name")
}
