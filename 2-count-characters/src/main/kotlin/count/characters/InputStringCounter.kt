package count.characters

class Counter {
  var length: Int = 0
  var word: String? = null

  init {
    print("What is the input string? ")
    this.reader()
  }

  fun reader() {
    this.word = readLine()
    this.length = this.word?.length ?: 0
  }

  fun retry() {
    println("Please enter some characters. ")
    this.reader()
  }

  fun result() {
    println("\"${this.word}\" has ${this.length} characters.")
  }
}
