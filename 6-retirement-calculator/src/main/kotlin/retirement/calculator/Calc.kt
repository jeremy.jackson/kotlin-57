package retirement.calculator

import java.time.LocalDateTime

class Calc {
  val now = LocalDateTime.now()
  val ageNow: Int
  val ageRetire: Int

  init {
    print("What is your current age? ")
    this.ageNow = reader() ?: 0
    print("At what age would you like to retire? ")
    this.ageRetire = reader() ?: 0
  }

  fun reader(): Int? {
    var num = rl()
    return num
  }

  fun rl(): Int? {
    var num: Int? = 0.toInt()
    try {
      num = readLine()?.toInt()
    } catch (e: NumberFormatException) {
      num = retry()
    }
    return num
  }

  fun retry(): Int? {
    print("Please provide a number: ")
    val num = rl()
    return num
  }

  fun say() {
    val (now, ageNow, ageRetire) = this

    val yearsLeft = ageRetire - ageNow
    val thisYear = now.getYear()
    val retireYear = thisYear + yearsLeft

    println("""
      |You have $yearsLeft years left until you can retire.
      |Its' $thisYear, so you can retire in $retireYear.
    """.trimMargin())
  }

  operator fun component1() = this.now
  operator fun component2() = this.ageNow
  operator fun component3() = this.ageRetire
}
