package mad.libs

class MadLib() {
  val noun: String
  val verb: String
  val adjective: String
  val adverb: String

  init {
    print("Enter a noun: ")
    this.noun = reader()
    print("Enter a verb: ")
    this.verb = reader()
    print("Enter an adjective: ")
    this.adjective = reader()
    print("Enter an adverb: ")
    this.adverb = reader()
  }

  fun reader(): String {
    val s = readLine() ?: ""
    return s
  }

  fun say() {
    val (noun, verb, adjective, adverb) = this
    println("Do you $verb your $adjective $noun $adverb? That's hilarious!")
  }

  operator fun component1(): String = this.noun
  operator fun component2(): String = this.verb
  operator fun component3(): String = this.adjective
  operator fun component4(): String = this.adverb
}
