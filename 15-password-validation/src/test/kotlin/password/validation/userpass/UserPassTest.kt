package password.validation.userpass

import password.validation.prompts.Prompts

import kotlin.test.Test
import kotlin.test.assertEquals

class UserPassTest {
    @Test fun testUserPass() {
        val prompt = Prompts("jj", "pp")
        val userpass = UserPass(prompt)
        assertEquals("jjpp", userpass.userpass, "prompts should match reference")
    }
}
