package password.validation.validate

import password.validation.prompts.Prompts
import password.validation.userpass.UserPass

import kotlin.test.Test
import kotlin.test.assertTrue

class ValidateTest {
    @Test fun testValidate() {
        val prompt = Prompts("jeremy", "password")
        val userpass = UserPass(prompt)
        val validated = Validate(userpass)
        assertTrue(validated.validated, "validated should be true")
    }
}
