package password.validation.prompts

import kotlin.test.Test
import kotlin.test.assertNotNull

class PromptsTest {
    @Test fun testAppHasUsername() {
        val classUnderTest = Prompts("jj", "pp")
        assertNotNull(classUnderTest.username, "app should have a greeting")
    }
    @Test fun testAppHasPassword() {
        val classUnderTest = Prompts("jj", "pp")
        assertNotNull(classUnderTest.password, "app should have a greeting")
    }
}

