package password.validation.userpass

import password.validation.prompts.Prompts

class UserPass(val prompts: Prompts) {
  val userpass: String

  init {
    userpass = prompts.username + prompts.password
  }
}
