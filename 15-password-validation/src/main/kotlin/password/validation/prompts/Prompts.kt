package password.validation.prompts

class Prompts(var username: String = "", var password: String = "") {
  init {
    if (username + password == "") {
      doPrompt()
    }
  }

  fun doPrompt() {
    print("What is your username? ")
    username = readLine()?.toString() ?: ""
    print("What is your password? ")
    password = readLine()?.toString() ?: ""
  }
}

