package password.validation.response

import password.validation.validate.Validate

class Respond(val validate: Validate) {
  init {
    if (validate.validated) {
      println("Welcome!")
    } else {
      println("I don't know you.")
    }
  }
}
