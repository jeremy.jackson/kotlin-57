package legal.driving.age.checkedage

import kotlin.test.Test
import kotlin.test.assertEquals

import legal.driving.age.prompt.Prompt

class CheckedAgeTest {
    @Test fun trueWhenGT16() {
        val prompt = Prompt(20)
        val classUnderTest = CheckedAge(prompt)
        assertEquals(true, classUnderTest.canDrive, "When > 16, canDrive == true.")
    }

    @Test fun trueWhenEQ16() {
        val prompt = Prompt(16)
        val classUnderTest = CheckedAge(prompt)
        assertEquals(true, classUnderTest.canDrive, "When == 16, canDrive == true.")
    }

    @Test fun trueWhenLT16() {
        val prompt = Prompt(15)
        val classUnderTest = CheckedAge(prompt)
        assertEquals(false, classUnderTest.canDrive, "When < 16, canDrive == true.")
    }
}
