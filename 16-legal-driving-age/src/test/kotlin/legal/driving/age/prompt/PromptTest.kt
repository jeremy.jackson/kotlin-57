package legal.driving.age.prompt

import kotlin.test.Test
import kotlin.test.assertEquals

class PromptTest {
    @Test fun promptShouldHaveAge() {
        val classUnderTest = Prompt(20)
        assertEquals(20, classUnderTest.age, "Prompt should have age equal to reference.")
    }
}
