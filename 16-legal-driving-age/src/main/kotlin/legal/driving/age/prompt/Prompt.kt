package legal.driving.age.prompt

class Prompt(var age: Int = -1) {
  init {
    if (age < 0) {
      print("What is your age? ")
      age = readLine()?.toInt() ?: 0.toInt()
    }
  }
}
