package legal.driving.age.response

import legal.driving.age.checkedage.CheckedAge

class Response(val checkedage: CheckedAge) {
  init {
    when (checkedage.canDrive) {
      true -> println("You are old enough to legally drive.")
      false -> println("You are not old enough to legally drive.")
    }
  }
}
