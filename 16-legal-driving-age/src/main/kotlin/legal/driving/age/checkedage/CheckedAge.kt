package legal.driving.age.checkedage

import legal.driving.age.prompt.Prompt

class CheckedAge(val prompt: Prompt) {
  val canDrive: Boolean

  init {
    if (prompt.age >= 16) {
      canDrive = true
    } else {
      canDrive = false
    }
  }
}
