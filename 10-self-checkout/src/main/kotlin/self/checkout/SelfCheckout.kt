package self.checkout

import kotlin.collections.MutableList

val TaxRate: Double = 0.055

data class Item(val price: Double, val quantity: Int)

class SelfCheckout {
  val items: MutableList<Item> = mutableListOf()
  var subtotal: Double = 0.toDouble()
  var tax: Double = 0.toDouble()
  var total: Double = 0.toDouble()

  init {
    for (i in 1..3) {
      print("Enter the price for item $i: ")
      val price: Double = readLine()?.toDouble() ?: 0.toDouble()
      print("Enter the quantity for item $i: ")
      val quantity: Int = readLine()?.toInt() ?: 0.toInt()
      items.add(Item(price, quantity))
    }
    calculate()
  }

  fun calculate() {
    val (items) = this
    this.subtotal = items
      .map { item -> item.price * item.quantity }
      .reduce { arr, cv -> arr + cv }
    this.tax = this.subtotal * TaxRate
    this.total = this.subtotal + this.tax
  }

  fun say() {
    val (_, subtotal, tax, total) = this
    println("""
      |Subtotal: $%.2f
      |Tax: $%.2f
      |Total: $%.2f
    """.trimMargin().format(subtotal, tax, total))
  }

  operator fun component1() = this.items
  operator fun component2() = this.subtotal
  operator fun component3() = this.tax
  operator fun component4() = this.total
}
