package pizza.party

import kotlin.math.floor

val SlicesPerPizza = 8

class PizzaParty {
  var people: Int = 0
  var pizzas: Int = 0
  var piecesPerPerson: Int = 0
  var leftoverPieces: Int = 0

  init {
    print("How many people? ")
    people = reader() ?: 0.toInt()
    print("How many pizzas do you have? ")
    pizzas = reader() ?: 0.toInt()
    calcPizzas()
  }

  fun reader(): Int? {
    var num = rl()
    return num
  }

  fun rl(): Int? {
    var num: Int? = 0.toInt()
    try {
      num = readLine()?.toInt()
    } catch (e: NumberFormatException) {
      num = retry()
    }
    return num
  }

  fun retry(): Int? {
    print("Please provide a number: ")
    val num = rl()
    return num
  }

  fun calcPizzas() {
    val (people, pizza) = this
    val allSlices = pizza.toFloat() * SlicesPerPizza.toFloat()
    this.piecesPerPerson = floor(allSlices / people.toFloat()).toInt()
    if (allSlices % people.toFloat() == 0.toFloat()) {
      this.leftoverPieces = 0
    } else {
      this.leftoverPieces = allSlices.toInt() - (this.piecesPerPerson * people)
    }
  }

  fun say() {
    val (people, pizzas, piecesPerPerson, leftoverPieces) = this
    println("""
      |$people people with $pizzas pizzas
      |Each person gets $piecesPerPerson pieces of pizza.
      |There are $leftoverPieces leftover pieces.
    """.trimMargin())
  }

  operator fun component1() = this.people
  operator fun component2() = this.pizzas
  operator fun component3() = this.piecesPerPerson
  operator fun component4() = this.leftoverPieces
}
