package compound.interest

import kotlin.math.pow

class CompoundInterest {
  val principal: Float
  val prerate: Float
  val rate: Float
  val years: Float
  val timesCompoundedPerYear: Float
  var amount: Float = 0.toFloat()

  init {
    print("What is the principal amount? ")
    principal = readLine()?.toFloat() ?: 0.toFloat()
    print("What is the rate? ")
    prerate = readLine()?.toFloat() ?: 0.toFloat()
    rate = prerate / 100
    print("What is the number of years? ")
    years = readLine()?.toFloat() ?: 0.toFloat()
    print("What is the number of times interest is compounded per year? ")
    timesCompoundedPerYear = readLine()?.toFloat() ?: 0.toFloat()
  }

  fun calculate() {
    amount = principal * ((1 + (rate / timesCompoundedPerYear)).pow(timesCompoundedPerYear * years))
  }

  fun say() {
    println("""
      |$%.2f invested at $prerate%% for %.0f years
      |compounded %.0f times per year is $%.2f.
    """.trimMargin().format(principal, years, timesCompoundedPerYear, amount))
  }
}
