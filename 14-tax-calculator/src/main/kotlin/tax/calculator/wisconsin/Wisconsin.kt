package tax.calculator.wisconsin

import tax.calculator.prompts.Prompts

class Wisconsin constructor(prompts: Prompts) {
  val rate: Double = 0.055
  val total: Double
  val tax: Double
  val subtotal: Double

  init {
    subtotal = prompts.amount
    tax = subtotal * rate
    total = subtotal + tax
  }

  fun say() {
    println("""
      |The subtotal is $%.2f.
      |The tax is $%.2f.
      |The total is $%.2f.
    """.trimMargin().format(subtotal, tax, total))
  }
}

