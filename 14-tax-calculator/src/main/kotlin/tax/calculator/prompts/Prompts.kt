package tax.calculator.prompts

class Prompts {
  val amount: Double
  val state: String

  init {
    print("What is the order amount? ")
    amount = readLine()?.toDouble() ?: 0.toDouble()
    print("What is the state? ")
    state = readLine()?.toString() ?: ""
  }
}
