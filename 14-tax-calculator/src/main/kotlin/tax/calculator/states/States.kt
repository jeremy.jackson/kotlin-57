package tax.calculator.states

import tax.calculator.prompts.Prompts

class States constructor(prompts: Prompts) {
  val total: Double

  init {
    total = prompts.amount
  }

  fun say() { println("The total is $%.2f.".format(total)) }
}
