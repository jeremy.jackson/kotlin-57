package paint.calculator

import kotlin.math.ceil

val SqFtCoveragePerGallon: Double = 350.toDouble()

class PaintCalc {
  var length: Int = 0
  var width: Int = 0
  var area: Int = 0
  var gallonsNeeded: Int = 0

  init {
    print("What is the length of the ceiling? ")
    length = reader() ?: 0
    print("What is the width of the ceiling? ")
    width = reader() ?: 0
    calc()
  }

  fun reader(): Int? {
    var num = rl()
    return num
  }

  fun rl(): Int? {
    var num: Int? = 0.toInt()
    try {
      num = readLine()?.toInt()
    } catch (e: NumberFormatException) {
      num = retry()
    }
    return num
  }

  fun retry(): Int? {
    print("Please provide a number: ")
    val num = rl()
    return num
  }

  fun calc() {
    val (length, width) = this
    this.area = length * width
    this.gallonsNeeded = ceil(this.area.toDouble() / SqFtCoveragePerGallon).toInt()
  }

  fun say() {
    val (_, _, area, gallonsNeeded) = this
    println("""
      |You will need to purchase $gallonsNeeded gallons of
      |paint to cover $area square feet.
    """.trimMargin())
  }

  operator fun component1() = this.length
  operator fun component2() = this.width
  operator fun component3() = this.area
  operator fun component4() = this.gallonsNeeded
}
